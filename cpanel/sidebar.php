<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style/sidebar.css">
    <link rel="stylesheet" href="../css/style/content.css">
    <link rel="stylesheet" href="../css/style/input.css">
    <link rel="stylesheet" href="../css/style/global/bootstrap.css">
    <!-- <link rel="shortcut icon" href="https://media.flaticon.com/dist/min/img/favicon.ico"> -->
    <script>
        function onClickMenu(menu) {
            var uri = window.location.href.split('/')
            var baseUri = "http:/"
            for (let i = 1; i < uri.length - 1; i++) {
                baseUri += uri[i] + '/';
            }
            window.location.replace(baseUri + menu + '.php')
        }
    </script>
</head>

<body>
    <div class="navigation">
        <div class="header-nav">
            <span class="icon">
                <img width="30px" src="../asset/logo.png" hidden="">
            </span>
            <span class="title-menu">
                <b>WEB-EL</b>
            </span>
            <a>
                <span class="toggle" onclick="toggleSideMenu()">
                    <i class='bx bx-menu'></i>
                </span>
            </a>
        </div>
        <ul>
   
          
<li class="list active">
    <a href="#" onclick="onClickMenu('dashboard')">
        <span class="icon">
            <i class='bx bxs-dashboard'></i>
        </span>
        <span class="title-menu">Dashboard</span>
    </a>
</li>


<li class="list" text="Logout" tAlign="right">
    <a href="#" onclick="onClickMenu('dosen')">
        <span class="icon">
            <i class='bx bxs-user-plus'></i>
        </span>
        <span class="title-menu">Dosen</span>
    </a>
</li>


<li class="list" text="Logout" tAlign="right">
    <a href="#" onclick="onClickMenu('fasilitas')">
        <span class="icon">
            <i class='bx bxs-school'></i>
        </span>
        <span class="title-menu">Fasilitas</span>
    </a>
</li>


<!-- menu pengumuman -->

<li class="list">
    <a href="#" onclick="onClickMenu('pengumuman')">
        <span class="icon">
            <i class='bx bxs-file-doc'></i>
        </span>
        <span class="title-menu">Pengumuman</span>
    </a>
</li>

<!-- akhir menu -->



            <a class="toggle-colapse" onclick="toggleSideMenu()">
                <span class="icon">
                    <i class='bx bx-right-arrow-alt'></i>
                </span>
            </a>
            <!--#endregion-->
        </ul>
        <div class="footer-nav">
            <a onclick="onClickMenu('setting')">
   
                 <i class='user bx bxs-user'></i>
   

    </a>
       
            <span class="username"></span>
            <a class="logout" href="../asset/function/logout.php" onclick="return confirm('Anda Yakin Ingin Keluar?')">
                <i class='bx bx-log-out' style="color: white;"></i>
            </a>
        </div>
    </div>

    <script src="../javascript/sidebar.js"> </script>
</body>

</html>