 <?php


    ?>


 <!DOCTYPE html>
 <html lang="en">

 <body>
     <?php
        include 'sidebar.php';




        ?>
     <title>Fasilitas</title>
     <div class="content">

<div class="header-content">
       <span class="title">Upload Fasilitas</span>
<hr style="width: 100px; margin-left: -1px; border:1px solid black;">
<a href="daftar-fasilitas.php">Daftar Fasilitas</a> / 
<br>
     </div>
    <br>
         <!-- <h2>Sub Header</h2> -->
         <form action="../asset/function/simpan-fasilitas.php" method="POST" enctype="multipart/form-data" id="form-upload">
             <div class="row">
                 <div class="col-xl-12">
                    <br>
                     <label for="fasilitas">Nama Fasilitas</label>
                     <input type="number" name="id_fasilitas" hidden="">
                     <input type="text" name="nama_fasilitas" id="fasilitas">
                     <br><br>
                     <div class="row">
                         <div class="col-xl-6">
                            <input id="profile-wakil" name="gambar1" type="file" style="display: none;">
                 <div class="container" style="text-align: center;">
                     <img id="img-wakil" class="img-thumbnail" width="300px" src="../images/fasilitas.jpg">
                     <div class="icon">
                         <a class="edit" onclick="onClickEditWakil()"><i class='bx bxs-file-plus'></i></a>
                         <a class="delete"><i class='bx bxs-trash'></i></a>
                     </div>
                 </div>
                         </div>
                         <div class="col-xl-6">
                        <input id="profile-pic" name="gambar2" type="file" style="display: none;">
                 <div class="container" style="text-align: center;">
                     <img id="img-profile" class="img-thumbnail" width="300px" src="../images/fasilitas.jpg">
                     <div class="icon">
                         <a class="edit" onclick="onClickEditProfile()"><i class='bx bxs-file-plus'></i></a>
                         <a class="delete"><i class='bx bxs-trash'></i></a>
                     </div>
                 </div>

                         </div>
                     </div>
                 </div>
             </div>
             <br>
             
             <div class="row">
                 <div class="col-xl-12">
                     <label for="deskripsi">Deskripsi Fasilitas</label>
                     <br><br>
                     <textarea type="text" id="deskripsi" name="deskripsi_fasilitas"></textarea>
                 </div>
                
<small> Note : <i> Jika foto fasiltas hanya 1, akan di masukan gambar default</i></small><br>
             </div>
             <button class="btn btn-success" type="submit" name="simpan"> <i class='bx bx-user-plus'></i> Simpan</button>
         </form>
     </div>

 <script src="ckeditor/ckeditor.js"></script>
  <script src="ckeditor/styles.js"></script>

  <script type="text/javascript">
    CKEDITOR.replace('deskripsi_fasilitas');
  </script>
      <script src="../javascript/profile.js"></script>

 </body>