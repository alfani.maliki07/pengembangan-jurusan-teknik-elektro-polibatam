 <?php
require "../asset/function/function.php";

    ?>


 <!DOCTYPE html>
 <html lang="en">

 <body>
     <?php
        include 'sidebar.php';




        //

        ?>
     <title> Tambah Dosen</title>
     <div class="content">

         
     <div class="header-content">
       <span class="title">Tambah Dosen</span>
<hr style="width: 100px; margin-left: -1px; border:1px solid black;">
<a href="dosen.php" >Kembali</a> /
          
     </div>
         <!-- <h2>Sub Header</h2> -->
         
         <form action="../asset/function/simpan-dosen.php" method="POST" enctype="multipart/form-data">
             <div class="row">
                 <input id="profile-pic" name="foto[]" type="file" style="display: none;" multiple="">
                 <div class="col-xl-4" style="text-align: center;">
                     <img id="img-profile" class="rounded" width="200px" src="../images/dosen/default.png">
                     <div class="icon">
                         <a class="edit" onclick="onClickEditProfile()"><i class='bx bxs-user-plus'></i></a>
                         <a class="delete"><i class='bx bxs-trash'></i></a>
                     </div>
                 </div>
                 <div class="col-xl-8">
                     <label for="nidn">NIDN</label>
                     <input type="number" name="nidn">

                     <label for="nama">Nama Dosen</label>
                     
                     <input type="text" name="nama" id="nama">
                     <div class="row">
                         <div class="col-xl-6">
                             <label for="email">Email</label>
                             <input type="email" name="email" id="email">
                         </div>
                         <div class="col-xl-6">
                             <label for="jabatan">Jabatan</label>
                             <input type="text" name="jabatan" id="jabatan">
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-xl-6">
                             <label for="prodi">Prodi</label>
                            <select name="prodi" class="custom-select">
							  <option selected>Silahkan Pilih Prodi</option>
							  <option value="Teknik Elektronika Manufaktur">
							  Teknik Elektronika Manufaktur
							  </option>
							  <option value="Teknik Elektronika">
							  Teknik Elektronika
							  </option>
							  <option value="Teknik Instrumentasi">
							  Teknik Instrumentasi
							  </option>
							  <option value="Teknik Mekatronika">
							  Teknik Mekatronika
							  </option>
							  <option value="Teknik Rekayasa Pembangkit Energi">
							  Teknik Rekayasa Pembangkit Energi
							  </option>
							  <option value="Teknik Rekayasa Robotika">
							  Teknik Rekayasa Robotika
							  </option>
							  
                          </select>
                         </div>
                         <div class="col-xl-6">
                             <label for="pangkat">Pangkat</label>
                             <input type="text" name="pangkat" id="pangkat">
                         </div>
                     </div>

                     <div class="row">
                         <div class="col-xl-6">
                             <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                             <input type="pendidikan_terakhir" name="pendidikan_terakhir" id="pendidikan_terakhir">
                         </div>
                         <div class="col-xl-6">
                             <label for="bidang">Bidang</label>
                             <input type="text" name="bidang" id="bidang">
                         </div>
                     </div>
                 </div>
             </div>
             <br>
        
             <div class="row">
                 <div class="col-xl-12">
                     <label for="riwayat">Riwayat Pendidikan</label>
                     <textarea type="text" id="riwayat" name="riwayat" rows="4"></textarea>
                 </div>
                 


                 </div>
             <button class="btn btn-success" type="submit" name="simpan"> <i class='bx bx-user-plus'></i> Simpan</button>
             </div>
             


         </form>
     </div>

     <script src="../javascript/profile.js"></script>

 </body>