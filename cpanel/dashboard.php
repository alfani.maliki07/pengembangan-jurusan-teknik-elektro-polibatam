<?php 
   session_start();
 if (isset($_SESSION['nama'])) {
  $nama_admin = $_SESSION['nama'];
 }else{
    header("Location:index.php");
 }



 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
</head>


<body>
 <?php  

   include 'sidebar.php';
   include "../asset/function/function.php";

//    jumlah dosen
   $r_dosen = $connection ->query("SELECT * FROM dosen");
   $d_dosen = mysqli_num_rows($r_dosen);

//    akhir jumlah dosen

// pengumuman 
$r_pengumuman = $connection ->query("SELECT * FROM pengumuman");
$d_pengumuman = mysqli_num_rows($r_pengumuman);
// akhir pengumuman 

// jumlah fasilitas 
$r_fasilitas = $connection -> query("SELECT * FROM fasilitas");
$d_fasilitas = mysqli_num_rows($r_fasilitas);
// akhir jumlah fasilitas 
    ?>
    <div class="content">
        <div class="header-content">
            <span class="title">Dashboard
                
            </span>
            <br>
 
            <!-- <span></span> -->
            <hr style="width: 100px; margin-left: -1px; border:1px solid black;">
     
    
        </div>

        <H3>Selamat Datang, <?php echo $nama_admin; ?></H3>

        <?php echo "<small> ".date('l, d  M  y')."<small>"; ?>
        <br>        <br>
        <div class="container">
        
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                    <div class="card border-primary shadow py-2">
                        <div class="card-body">
                            <h4 class="card-title">Dosen</h4>
                         <h1 class="h1"> <?php echo $d_dosen; ?></h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                    <div class="card border-primary shadow py-2">
                        <div class="card-body">
                         
                        <h4 class="card-title">Pengumuman</h4>
                        
                      <h1 class="h1">  <?php echo $d_pengumuman; ?></h1>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12">
                    <div class="card border-primary shadow py-2">
                        <div class="card-body">
                        <h4 class="card-title">Fasilitas</h4>
                        <h1 class="h1">  <?php echo $d_fasilitas; ?></h1>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <?php
        // include '../function/chart.php'
        ?>
    </div>

</body>