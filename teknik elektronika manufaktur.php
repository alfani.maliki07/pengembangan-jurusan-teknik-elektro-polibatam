<?php 
 


 ?>

 <!DOCTYPE html>
<html>
<head>
	 <head>

 	  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">
  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">

  <!-- font awesome style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />
 	<title>D3 Teknik Elektronika Manufaktur</title>
</head>




<body>
<!-- header -->

<header>
    <a href="" class="logo">T-EL</a>
    <div class="navigation">
      <ul class="menu">
        <div class="close-btn"></div>
        <li class="menu-item"><a href="">Pengumuman</a></li>
        <li class="menu-item"><a href="index.php">Home</a></li>
        <li class="menu-item">
          <a class="sub-btn" href="#">Fasilitas <i class="fas fa-angle-down"></i></a>
          <ul class="sub-menu">
            <li class="sub-item more">
              <a class="more-btn" href="#">Workspace <i class="fas fa-angle-right"></i></a>
              <ul class="more-menu">
                <li class="more-item"><a href="">Workspace 1</a></li>
                <li class="more-item"><a href="">Workspace 1</a></li>
                <li class="more-item"><a href="">Workspace 1</a></li>
              </ul>
            </li>
            <li class="sub-item"><a href="">Studio</a></li>
          </ul>
        </li>
        <li class="menu-item">
          <a class="sub-btn" href="#">Program Studi <i class="fas fa-angle-down"></i></a>
          <ul class="sub-menu">
            <li class="sub-item"><a href="teknik elektronika manufaktur.php">D3 Teknik Elektronika Manufaktur</a></li>
            <li class="sub-item"><a href="teknik elektronika.php">D3 Teknik Elektronika</a></li>
            <li class="sub-item"><a href="">D3 Teknik Instrumentasi</a></li>
            <li class="sub-item"><a href="">D4 Teknik Mekatronika</a></li>
            <li class="sub-item"><a href="">D4 Teknologi Rekayasa Pembangkit Energi</a></li>
            <li class="sub-item"><a href="">D4 Teknologi Rekayasa Robotika</a></li>
          </ul>
        </li>
        <li class="menu-item"><a href="dosen.php">Dosen</a></li>
        <li class="menu-item"><a href="prestasi.php">Prestasi</a></li>
      </ul>
    </div>
  </header>
  <!-- akhir header -->
<br>
<br>
<br>

  <!-- heading -->

<h3 class="h3 text-center"><b>
Teknik Elektronika Manufaktur</b>
</h3>
<hr style="width: 100px; border:1px solid black;">
<p class="p text-center" size>
Teknik Elektronika adalah bidang teknik yang mempelajari tentang komponen listrik dan peralatan-peralatan semi konduktor. 	
</p>
<!-- akhir heading -->
<br>
<br>


<h3 class="h3 text-center"><b>CAPAIAN PEMBELAJARAN</b></h3>
<p class="p text-center" size>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
































































    <!-- footer section -->
    <section class="footer_section">
      <div class="container">
        <p>
          &copy; <span id="displayYear"></span> Politeknik Negeri Batam
        </p>
      </div>
    </section>
    <!-- footer section -->
    <!-- jQery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script>
      $(document).ready(function () {
        $(".sub-btn").click(function () {
          $(this).next(".sub-menu").slideToggle();
        });

        $(".more-btn").click(function () {
          $(this).next(".more-menu").slideToggle();
        });
      });
    </script>
    <script>
      window.addEventListener("scroll", function () {
        var header = document.querySelector("header");
        header.classList.toggle("sticky", window.scrollY > 0);
      })
    </script>
    <!-- popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- owl slider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
    </script>
    <!-- End Google Map -->














































































</body>
</html>