<?php 
require "asset/function/function.php";


// fasilitas
$r_fasilitas = $connection ->query("SELECT * FROM fasilitas ORDER BY id_fasilitas DESC LIMIT 6");
// akhir fasilitas



 ?>



<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

  <title> EL - POLIBATAM </title>

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">

  <!-- font awesome style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />


  <style>
    .bfasilitas{
      color: #011351;
    }
    .bfasilitas:hover{
      color: rgba(1, 19, 81,0.6);
    }
  </style>
</head>

<body>
  <!--<header>
    <a href="" class="logo">T-EL</a>
    <ul>
      <li>
        <a href="" class="nav-link">Pengumuman</a>
      </li>
      <li class="nav-item active">
        <a href="index.html" class="nav-link">Home</a>
      </li>
      <li>
        <a href="about.html" class="nav-link">Fasilitas</a>
          <ul>
            <li><a href="">Workspace</a></li>
            <li><a href="">Studio</a></li>
          </ul>
      </li>
      <li>
        <a href="service.html" class="nav-link">Program Studi</a>
          <ul>
            <li><a href="">Teknik Elektronika Manufaktur</a></li>
            <li><a href="">Teknik Elektronika</a></li>
            <li><a href="">Teknik Instrumentasi</a></li>
            <li><a href="">Teknik Mekatronika</a></li>
            <li><a href="">Teknologi Rekayasa Pembangkit Energi</a></li>
            <li><a href="">Teknologi Rekayasa Robotika</a></li>
          </ul>
      </li>
      <li><a href="" class="nav-link">Dosen</a></li>
      <li><a href="" class="nav-link">Prestasi</a></li>
    </ul>
  </header>-->
<?php include "asset/function/header.php" ?>


  <div class="hero_area">

    <div class="hero_bg_box">
      <img src="images/hero-bg.png" alt="">
    </div>



    <!-- header section strats -->
    <!--<header class="header_section">
      <div class="container">
        <div class="header">
          <nav class="navbar navbar-expand-lg custom_nav-container ">
            <a class="navbar-brand" href="index.html">
              <span>
                Homep
              </span>
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="about.html"> About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="service.html">Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="contact.html">Contact Us</a>
                </li>
                <form class="form-inline">
                  <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                    <i class="fa fa-search" aria-hidden="true"></i>
                  </button>
                </form>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </header>-->
    <!-- end header section -->
    <!-- slider section -->
    <section class="slider_section ">
      <div id="customCarousel1" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="container ">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="detail-box">
                    <h1>
                    JURUSAN ELEKTRONIKA
                    </h1>
                    <p>
                      Lulusan program ini memiliki kapasitas dalam bidang process/equipment technician (IC packaging industry), PCB/SMT technician, failure analysis (FA)/product analysis (PA) technician dan RF tester, sesuai dengan perkembangan teknologi elektronik sedang berkembang membutuhkan tenaga ahli dan manufaktur elektronik yang terampil di industri.
                    </p>
              
                  </div>
                </div>
              
              </div>
            </div>
          </div>
          <div class="carousel-item ">
            <div class="container ">
              <div class="row">


                <div class="col-md-6 ">
                  <div class="detail-box">
                    <h1>
            
                    </h1>
                    <p>
                      Lorem ipsum is placeholder text commonly used in the graphic, print,
                      and publishing industries for previewing layouts and visual mockups.
                    </p>
                    <div class="btn-box">
                      <a href="" class="btn1">
                        Read More
                      </a>
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="img-box">
                    <img src="images/slider-img.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container ">
              <div class="row">
                <div class="col-md-6 ">
                  <div class="detail-box">
                    <h1>
                      Teknik Elektro
                    </h1>
                    <p>
                      Lorem ipsum is placeholder text commonly used in the graphic, print,
                      and publishing industries for previewing layouts and visual mockups.
                    </p>
                    <div class="btn-box">
                      <a href="" class="btn1">
                        Read More
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="img-box">
                    <img src="images/slider-img.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="col-md-6 ml-auto">
            <div class="carousel_btn-box">
              <a class="carousel-control-prev" href="#customCarousel1" role="button" data-slide="prev">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#customCarousel1" role="button" data-slide="next">
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- end slider section -->
  </div>


  <!-- about section -->

  <section class="about_section layout_padding">
    <div class="container">
      <div class="detail-box">
        <div class="heading_container heading_center">
          <h2>
            <img src="images/home.png" alt=""> <span>About Us</span>
          </h2>
        </div>
        <p>
           Teknik Elektro adalah salah satu bidang ilmu teknik mengenai aplikasi listrik untuk memenuhi kebutuhan masyarakat. Teknik listrik melibatkan konsep, perancangan, pengembangan, dan produksi peralatan listrik dan elektronik yang dibutuhkan oleh masyarakat.
        </p>
      </div>
      <div class="col-md-9 mx-auto px-0">
        <div class="img-box">
          <img src="images/about-img.png" alt="" style="width: 50%;">
        </div>
      </div>
    </div>

    <!-- end about section -->

    <!-- portfolio section -->

    <div class="portfolio_section layout_padding" id="prodi">
      <div class="container">
     <div class="heading_container heading_center">
          <h2>
            <img src="images/home.png" alt=""> <span> PROGRAM STUDI</span>
          </h2>
          <p>
          Pogram Studi jurusan Elektronika Polibatam          
          </p>
        </div>
        <div class="layout_padding2-top" style="margin-top: -40px;">
          <div class="row">
            
            <div class="col-md-4 col-sm-6">
              <span>Teknik Mekatronika</span>
              <div class="img-box">
                <img src="images/logo-meka.jpeg" alt="">
                <a href="program-studi.php?prodi=4">
                  <img src="images/link.png" alt="">
                </a>
              </div>
            </div>
         
            <div class="col-md-4 col-sm-6">
              <span>Teknik Elektronika</span>
              <div class="img-box" >
                <img src="images/energi.jpg" alt="">
                <a href="program-studi.php?prodi=2">
                  <img src="images/link.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <span>Teknik Elektronika Manufaktur</span>
              <div class="img-box">
                <img src="images/logo-manufaktur.jpg" alt="">
                <a href="program-studi.php?prodi=1">
                  <img src="images/link.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <span>Diploma 3 Teknik Instrumentasi</span>
              <div class="img-box">
                <img src="images/intrumentasi.jpg" alt="">
                <a href="program-studi.php?prodi=3">
                  <img src="images/link.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <span class="h6">Sarjana Terapan Teknologi Rekayasa Pembangkit Energi</span>
              <div class="img-box">
                <img src="images/logo-pembangkit.jpg" alt="" style="height: 220px;">
                <a href="program-studi.php?prodi=5">
                  <img src="images/link.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <span>Sarjana Terapan Teknik Robotika</span>
              <div class="img-box">
                <img src="images/robotika.jpeg" alt="" style="height: 240px;">
                <a href="program-studi.php?prodi=6">
                  <img src="images/link.png" alt="">
                </a>
              </div>
            </div>
          </div>
        </div>
    
      </div>
    </div>

    <!-- end portfolio section -->
  </section>
  <!-- service section -->

  <section class="service_section layout_padding-bottom" id="fasilitas">
    <div class="service_container">
      <div class="container ">
        <div class="heading_container heading_center">
          <h2>
            <img src="images/home.png" alt=""> <span> FASILITAS</span>
          </h2>
          <p>
           Fasilitas jurusan Elektronika untuk menunjang perkuliahan
          </p>
          <br>
        </div>
        <div class="row">

        <?php while($d_fasilitas = $r_fasilitas ->fetch_assoc()): ?>
          <div class="col-md-4 ">
            <div class="card ">
          
                <img src="images/fasilitas/<?php echo $d_fasilitas['gambar1'] ?>" alt="">
        
              <div class="card-body">
                <h5 class="h5 text-center">
                <?php echo $d_fasilitas['nama_fasilitas'] ?>
                </h5>
                
                <a href="detail-fasilitas.php?id_fasilitas=<?php echo $d_fasilitas['id_fasilitas'] ?>" class="bfasilitas">
                 <b> Read More </b>
                </a>
              </div>
            </div>
          </div>
          <?php endwhile; ?>




        </div>
        <!--<div class="btn-box">
          <a href="">
            View All
          </a>
        </div>-->
      </div>
    </div>
  </section>

  <!-- end service section -->

  <!-- why us section -->

  <!--<section class="why_us_section ">
    <div class="why_bg_box">
      <img src="images/why-bg.jpg" alt="">
    </div>
    <div class="why_us_container">
      <div class="container">
        <div class="box_container">
          <div class="row">
            <div class="col-sm-6 col-md-4 mx-auto">
              <div class="box">
                <div class="img-box">
                  <img src="images/w1.png" alt="">
                </div>
                <div class="detail-box">
                  <div class="num-box">
                    <span>
                      10+
                    </span>
                  </div>
                  <h5>
                    Years Experience
                  </h5>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 mx-auto">
              <div class="box">
                <div class="img-box">
                  <img src="images/w2.png" alt="">
                </div>
                <div class="detail-box">
                  <div class="num-box">
                    <span>
                      246
                    </span>
                  </div>
                  <h5>
                    Happy Clients
                  </h5>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 mx-auto">
              <div class="box">
                <div class="img-box">
                  <img src="images/w3.png" alt="">
                </div>
                <div class="detail-box">
                  <div class="num-box">
                    <span>
                      1278
                    </span>
                  </div>
                  <h5>
                    Systems Installed
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>-->

  <!-- end why us section -->

  <!-- contact section -->
  <!--
  <section class="contact_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2>
          <img src="images/home.png" alt=""> <span> Contact Us </span>
        </h2>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form_container contact-form">
            <form action="">
              <div>
                <input type="text" placeholder="Your Name" />
              </div>
              <div>
                <input type="text" placeholder="Phone Number" />
              </div>
              <div>
                <input type="email" placeholder="Email" />
              </div>
              <div>
                <input type="text" class="message-box" placeholder="Message" />
              </div>
              <div class="btn_box">
                <button>
                  Submit Now
                </button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-6">
          <div class="map_container">
            <div class="map">
              <div id="googleMap"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>-->
  <!-- end contact section -->

  <!-- client section -->

  <!--<section class="client_section layout_padding-bottom">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          <img src="images/home.png" alt=""> <span> What says our Clients</span>
        </h2>
      </div>
      <div class="carousel-wrap ">
        <div class="owl-carousel client_owl-carousel">
          <div class="item">
            <div class="box">
              <div class="img-box">
                <img src="images/c1.jpg" alt="" class="box-img">
              </div>
              <div class="detail_container">
                <div class="detail-box">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis
                  </p>
                  <h6 class="rating">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </h6>
                </div>
                <h5>
                  Alan Champ
                </h5>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="box">
              <div class="img-box">
                <img src="images/c2.jpg" alt="" class="box-img">
              </div>
              <div class="detail_container">
                <div class="detail-box">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis
                  </p>
                  <h6 class="rating">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </h6>
                </div>
                <h5>
                  Maya Jonas
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>-->

  <!-- end client section -->

  <!-- end contact section -->

  <div class="footer_bg">



    <!-- info section -->
    <section class="info_section ">
      <div class="container">
        <div class="row">
          <div class="col-md-3 mb-4 mb-md-0 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_info">
              <h5>
                Informations
              </h5>
              <p>
                ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
              </p>
            </div>
          </div>
          <div class="col-md-3 mb-4 mb-md-0 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_contact">
              <h5>
                About
              </h5>
              <div>
                <div class="img-box">
                  <img src="images/location-white.png" width="18px" alt="">
                </div>
                <p>
                  Address
                </p>
              </div>
              <div>
                <div class="img-box">
                  <img src="images/telephone-white.png" width="12px" alt="">
                </div>
                <p>
                  +01 1234567890
                </p>
              </div>
              <div>
                <div class="img-box">
                  <img src="images/envelope-white.png" width="18px" alt="">
                </div>
                <p>
                  demo@gmail.com
                </p>
              </div>
            </div>
          </div>
          <!-- <div class="col-md-3 mb-4 mb-md-0 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_insta">
              <h5>
                Instagram
              </h5>
              <div class="insta_container">
                <div>
                  <a href="">
                    <div class="insta-box b-1">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                  <a href="">
                    <div class="insta-box b-2">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                </div>

                <div>
                  <a href="">
                    <div class="insta-box b-3">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                  <a href="">
                    <div class="insta-box b-4">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                </div>
                <div>
                  <a href="">
                    <div class="insta-box b-3">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                  <a href="">
                    <div class="insta-box b-4">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_form ">
              <h5>
                Newsletter
              </h5>
              <form action="">
                <input type="email" placeholder="Enter your email">
                <button>
                  Subscribe
                </button>
              </form>
              <--<div class="social_box">
                <a href="">
                  <img src="images/fb.png" alt="">
                </a>
                <a href="">
                  <img src="images/twitter.png" alt="">
                </a>
                <a href="">
                  <img src="images/linkedin.png" alt="">
                </a>
                <a href="">
                  <img src="images/youtube.png" alt="">
                </a>
              </div>
            </div>
          </div>-->
        </div>
      </div>
    </section>

    <!-- end info_section -->

    <!-- footer section -->
    <section class="footer_section">
      <div class="container">
        <p>
          &copy; <span id="displayYear"></span> Politeknik Negeri Batam
        </p>
      </div>
    </section>
    <!-- footer section -->
    <!-- jQery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script>
      $(document).ready(function () {
        $(".sub-btn").click(function () {
          $(this).next(".sub-menu").slideToggle();
        });

        $(".more-btn").click(function () {
          $(this).next(".more-menu").slideToggle();
        });
      });
    </script>
    <script>
      window.addEventListener("scroll", function () {
        var header = document.querySelector("header");
        header.classList.toggle("sticky", window.scrollY > 0);
      })
    </script>
    <!-- popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- owl slider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
    </script>
    <!-- End Google Map -->

</body>

</html>