<?php 




if (isset($_GET['prodi'])) {
	$prodi = $_GET['prodi'];
}else{
	header("Location:../index.php");
}
 ?>

<?php 
switch ($prodi) {
	case 1:
		?>
<!-- Elektronika Manufaktur -->
<title>Elektronika Manufaktur - POLIBATAM</title>

<div class="jumbotron jumbotron-fluid" style="background-image: url(images/elektromanuk.jpg); background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    ">
	  <br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br>
	</div>
  <div class="container">
  	<h1 align="center">Diploma 3 Teknik Elektronika Manufaktur</h1><hr><br>

<div class="row">
	
<div class="col-sm-6">
	
	<h4 class="h4">KOMPETENSI UTAMA</h4>
<p class="p">Lulusan program ini memiliki kapasitas dalam bidang process/equipment technician (IC packaging industry), PCB/SMT technician, failure analysis (FA)/product analysis (PA) technician dan RF tester, sesuai dengan perkembangan teknologi elektronik sedang berkembang membutuhkan tenaga ahli dan manufaktur elektronik yang terampil di industri.</p>
</div>

<div class="col-sm-6">

</div>



</div>

  	
  <br>

  <h4 class="h4">PROSPEK KERJA</h4>

  <ol type="1">
  	<li>
Process/equipment technician (IC Packaging Industry)
  	</li>

  	<li>
  	PCB/SMT technician
  	</li>

  	<li>
 Failure analysis (FA)/product analysis (PA) technician
  	</li>

  	<li>
  RF tester.
  	</li>

  
  	</ol>
<br>



  <h4 class="h4">KOMPETENSI LULUSAN</h4>

  <ul >
  	<li>
Mampu membuat perancangan sistem kelistrikan sederhana, sistem kendali maupun sistem instrumentasi mulai dari merencanakan gambar skematik, membuat perencanaan anggaran, pemilihan peralatan dan komponen, sesuai standard yang ada
  	</li>

  	<li>
Mampu memastikan akurasi dan reliabilitas sistem kelistrikan, sistem kendali dan sistem instrumentasi serta sistem keamanannya
  	</li>

  	<li>
Mampu mengidentifikasi sumber masalah (troubleshooting) sistem kelistrikan dan sistem otomasi
  	</li>

  	<li>
Mampu membaca gambar sistem otomasi mulai dari sistem pneumatik, sistem elektrikal, mekanik,hingga gambar sistem kendali yang komplek
  	</li>

  	<li>
Mampu melakukan pemrograman kendali seperti PLC, Microcontroller
  </li>

    	<li>
Mampu membaca dan mengimplementasikan electrical wiring untuk industri
  	</li>

    	<li>
Mampu melakukan debuging test equipment
  	</li>

  	<li>
Mampu mengoperasikan semua peralatan elektronik tes dan instrumentasi seperti osciloscope, multimeter, serta beberapa standard tes sistem kendali
  </li>
  	</ul>
<br>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Kurikulum-D3-Teknik-Elektronika-Polibatam.pdf"> <i class="fa-solid fa-download"></i> Download Kurikulum
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Sertifikat-Akreditasi-EL-2020-2025.pdf"> <i class="fa-solid fa-download"></i> Download Akreditasi
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Elekronika-Manufaktur-1.pdf">
	<i class="fa-solid fa-download"></i> Download Mata Kuliah
</a>






<br><br>
<?php break;?>
 <!-- akhir elektromanufaktur -->


<?php

	case 2:
		?>
<!-- Elektronika -->
<title>Teknik Elektronika - POLIBATAM</title>
<div class="jumbotron jumbotron-fluid" style="background-image: url(images/elektro.jpg);background-position: center;
    background-repeat: no-repeat;
    background-size: cover;">
	  <br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br>

	  
	</div>
  <div class="container">
  	<h1 align="center">Diploma 3 Teknik Elektronika </h1><hr><br>

<div class="row">
	
<div class="col-sm-6">
	
	<h4 class="h4">KOMPETENSI UTAMA</h4>
<p class="p">Lulusan program ini memiliki kapasitas dalam bidang membuat dan membaca gambar teknik sesuai standarisasi gambar teknik, membuat pemrograman dasar, merencanakan, merancang, dan menguji perangkat kendali di bidang manufaktur.</p>
</div>

<div class="col-sm-6">

</div>



</div>

  	
  <br>

  <h4 class="h4">PROSPEK KERJA</h4>

  <ol type="1">
  	<li>
Automation control
  	</li>

  	<li>
  Programming
  	</li>

  	<li>
 Detail drafter dalam bidang mekanik dan kelistrikan dan mampu untuk membuka usaha sendiri di bidang elektronika.
  	</li>

  
  	</ol>
<br>



  <h4 class="h4">KOMPETENSI LULUSAN</h4>

  <ul >
  	<li>
Mampu membuat perancangan sistem kelistrikan sederhana, sistem kendali maupun sistem instrumentasi mulai dari merencanakan gambar skematik, membuat perencanaan anggaran, pemilihan peralatan dan komponen, sesuai standard yang ada
  	</li>

  	<li>
Mampu memastikan akurasi dan reliabilitas sistem kelistrikan, sistem kendali dan sistem instrumentasi serta sistem keamanannya
  	</li>

  	<li>
Mampu mengidentifikasi sumber masalah (troubleshooting) sistem kelistrikan dan sistem otomasi
  	</li>

  	<li>
Mampu membaca gambar sistem otomasi mulai dari sistem pneumatik, sistem elektrikal, mekanik,hingga gambar sistem kendali yang komplek
  	</li>

  	<li>
Mampu melakukan pemrograman kendali seperti PLC, Microcontroller
  </li>

    	<li>
Mampu membaca dan mengimplementasikan electrical wiring untuk industri
  	</li>

    	<li>
Mampu melakukan debuging test equipment
  	</li>

  	<li>
Mampu mengoperasikan semua peralatan elektronik tes dan instrumentasi seperti osciloscope, multimeter, serta beberapa standard tes sistem kendali
  </li>
  	</ul>
<br>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Kurikulum-D3-Teknik-Elektronika-Polibatam.pdf"> <i class="fa-solid fa-download"></i> Download Kurikulum
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Sertifikat-Akreditasi-EL-2020-2025.pdf"> <i class="fa-solid fa-download"></i> Download Akreditasi
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Elekronika-Manufaktur-1.pdf">
	<i class="fa-solid fa-download"></i> Download Mata Kuliah
</a>






<br><br>
<?php break;?>
 <!-- akhir elektronika -->
<?php    
  case 3:
    ?>
<!-- Instrumentasi -->
<title>Teknik Instrumentasi - POLIBATAM</title>
<div class="jumbotron jumbotron-fluid" style="background-image: url(images/teknik-instrumentasi.jpg);background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    margin-top: 20px;">
    <br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br>

    
  </div>
  <div class="container">
    <h1 align="center">Diploma 3 Teknik Instrumentasi </h1><hr><br>

<div class="row">
  
<div class="col-sm-6">
  
  <h4 class="h4">KOMPETENSI UTAMA</h4>
<p class="p">Lulusan program ini memiliki kapasitas dalam bidang instalasi dan merawat distributed control system (DCS), programmable logic control (PLC), supervisory control dan data acquisition (SCADA) dan jaringan kontrol proses berbasiskan fieldbus.</p>
</div>

<div class="col-sm-6">

</div>



</div>

    
  <br>

  <h4 class="h4">PROSPEK KERJA</h4>

  <ol type="1">
    <li>
Teknisi instrumentasi dan control
    </li>

    <li>
 Teknisi instrumentasi proses
    </li>


  
    </ol>
<br>



  <h4 class="h4">KOMPETENSI LULUSAN</h4>

  <ul >
    <li>
Mampu melakukan perbaikan peralatan mekanik, hidrolik, pneumatik, otomasi dan elektronika menurut prosedur di lapangan
    </li>

    <li>
Mampu menggunakan peralatan pengujian elektronika (seperti: osiloskop, multimeter, dan lain-lain) dan hand tools untuk melakukan troubleshooting dan perbaikan peralatan
    </li>

    <li>
Mampu melakukan perbaikan yang berhubungan pengukuran proses industri
    </li>

    <li>
Mampu melakukan inspeksi peralatan dan sistem untuk mendiagnosis kegagalan dengan menggunakan peralatan pengujian
    </li>

    <li>
Mampu menggambar piping and instrumentation diagram (P&ID) dengan berbantu computer aided design (CAD) dengan standar ISA
  </li>

      <li>
Mampu menggunakan sistem terkomputerisasi (sebagai kontrol dan antarmuka)
    </li>

      <li>
Mampu melakukan kalibrasi peralatan sesuai dengan prosedur
    </li>

    <li>
Mampu melakukan instalasi dan merawat distributed control system (DCS), programmable logic control (PLC), supervisory control dan data acquisition (SCADA), dan jaringan kontrol proses berbasiskan fieldbus
  </li>
    </ul>
<br>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Kurikulum-D3-Teknik-Elektronika-Polibatam.pdf"> <i class="fa-solid fa-download"></i> Download Kurikulum
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Sertifikat-Akreditasi-EL-2020-2025.pdf"> <i class="fa-solid fa-download"></i> Download Akreditasi
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Elekronika-Manufaktur-1.pdf">
  <i class="fa-solid fa-download"></i> Download Mata Kuliah
</a>






<br><br>
<?php break;?>








<?php    
  case 4:
    ?>
<!-- Mekatronika -->
<title>Teknik Mekatronika - POLIBATAM</title>
<div class="jumbotron jumbotron-fluid" style="background-image: url(images/mekatronika.jpg);background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    margin-top: 20px;">
    <br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br>  

    
  </div>
  <div class="container">
    <h1 align="center">Sarjana Terapan Teknik Mekatronika </h1><hr><br>

<div class="row">
  
<div class="col-sm-6">
  
  <h4 class="h4">KOMPETENSI UTAMA</h4>
<p class="p">Program Sarjana Terapan Teknik Mekatronika membekali calon lulusan untuk menjadi automation engineer, automation technician, test Enginner, electro-mechanical drafter, dan project engineer dengan mengacu pada KKNI tahun 2012 di level KKNI 6 (enam).</p>
</div>

<div class="col-sm-6">

</div>



</div>

    
  <br>

  <h4 class="h4">PROSPEK KERJA</h4>

  <ol type="1">
    <li>
Teknisi instrumentasi dan control
    </li>

    <li>
 Teknisi instrumentasi proses
    </li>


  
    </ol>
<br>



  <h4 class="h4">KOMPETENSI LULUSAN</h4>

  <ul >
    <li>
Mampu melakukan perbaikan peralatan mekanik, hidrolik, pneumatik, otomasi dan elektronika menurut prosedur di lapangan
    </li>

    <li>
Mampu menggunakan peralatan pengujian elektronika (seperti: osiloskop, multimeter, dan lain-lain) dan hand tools untuk melakukan troubleshooting dan perbaikan peralatan
    </li>

    <li>
Mampu melakukan perbaikan yang berhubungan pengukuran proses industri
    </li>

    <li>
Mampu melakukan inspeksi peralatan dan sistem untuk mendiagnosis kegagalan dengan menggunakan peralatan pengujian
    </li>

    <li>
Mampu menggambar piping and instrumentation diagram (P&ID) dengan berbantu computer aided design (CAD) dengan standar ISA
  </li>

      <li>
Mampu menggunakan sistem terkomputerisasi (sebagai kontrol dan antarmuka)
    </li>

      <li>
Mampu melakukan kalibrasi peralatan sesuai dengan prosedur
    </li>

    <li>
Mampu melakukan instalasi dan merawat distributed control system (DCS), programmable logic control (PLC), supervisory control dan data acquisition (SCADA), dan jaringan kontrol proses berbasiskan fieldbus
  </li>
    </ul>
<br>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Kurikulum-D3-Teknik-Elektronika-Polibatam.pdf"> <i class="fa-solid fa-download"></i> Download Kurikulum
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Sertifikat-Akreditasi-EL-2020-2025.pdf"> <i class="fa-solid fa-download"></i> Download Akreditasi
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Elekronika-Manufaktur-1.pdf">
  <i class="fa-solid fa-download"></i> Download Mata Kuliah
</a>






<br><br>
<?php break;?>



<?php    
  case 6:
    ?>
<!-- Energi -->
<title>Teknik Robotika - POLIBATAM</title>
<div class="jumbotron jumbotron-fluid" style="background-image: url(images/teknik-robotika.jpg);background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    margin-top: 20px;">
    <br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br>

    
  </div>
  <div class="container">
    <h1 align="center">Sarjana Terapan Teknik Robotika </h1><hr><br>

<div class="row">
  
<div class="col-sm-12">
  
  <h4 class="h4">KOMPETENSI UTAMA</h4>
<p class="p">Graduates of this program have the capacity in designing automated robotic systems used to increase production and precision levels in specific industries, troubleshooting robotic systems, and designing software to control robotic systems.</p>
</div>

<div class="col-sm-6">

</div>



</div>

    
  <br>

  <h4 class="h4">PROSPEK KERJA</h4>

  <ol type="1">
    <li>
Robotic engineer
    </li>

    <li>
 Robotic technician
    </li>
   

  
    </ol>
<br>



  <h4 class="h4">KOMPETENSI LULUSAN</h4>

  <ul >
    <li>
Mampu menerapkan matematika, sains, dan prinsip-prinsip rekayasa ke dalam prosedur, proses, sistem, atau metodologi rekayasa terapan untuk memecahkan masalah rekayasa yang didefinisikan secara luas di bidang robotika;
    </li>

    <li>
Mampu mengidentifikasi, merumuskan, melakukan pencarian referensi/standar/kode/database, menganalisis, dan memecahkan masalah rekayasa umum di bidang robotika dengan menggunakan alat analisis untuk satu bidang spesialisasi dengan mempertimbangkan ekonomi, kesehatan dan keselamatan masyarakat, budaya, sosial , dan faktor lingkungan (environmental concern);
    </li>

    <li>
Mampu merancang dan mewujudkan komponen, proses, peralatan, fasilitas atau instalasi, desain sistem rekayasa yang terdefinisi dengan baik di bidang robotika, dan bagian dari desain sistem rekayasa yang didefinisikan secara luas di bidang robotika, yang memenuhi kebutuhan khusus dengan pertimbangan yang tepat dari kesehatan masyarakat dan masalah keselamatan, budaya, sosial, dan lingkungan dengan mengacu pada standar dan metode industri;
    </li>

    <li>
Mampu memilih sumber daya dan memanfaatkan desain teknik dan alat analisis di bidang robotika dan komputasi berbasis teknologi informasi yang mengacu pada standar dan metode industri;
    </li>

    <li>
Mampu meningkatkan kinerja atau kualitas suatu proses di bidang robotika melalui pengujian, pengukuran benda kerja, analisis, dan interpretasi data sesuai prosedur dan standar;
  </li>

      <li>
Mampu menggunakan teknologi modern dalam melaksanakan pekerjaan di bidang robotika.    </li>

    </ul>
<br>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Kurikulum-D3-Teknik-Elektronika-Polibatam.pdf"> <i class="fa-solid fa-download"></i> Download Kurikulum
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Sertifikat-Akreditasi-EL-2020-2025.pdf"> <i class="fa-solid fa-download"></i> Download Akreditasi
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Elekronika-Manufaktur-1.pdf">
  <i class="fa-solid fa-download"></i> Download Mata Kuliah
</a>






<br><br>
<?php break;?>
<?php    
  case 5:
    ?>
<!-- Energi -->
<title>Teknik Teknologi Rekayasa Pembangkit Energi - POLIBATAM</title>
<div class="jumbotron jumbotron-fluid" style="background-image: url(images/jurusan-energi.jpg);background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    margin-top: 20px;">
    <br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br>

    
  </div>
  <div class="container">
    <h1 align="center">Sarjana Terapan Teknologi Rekayasa Pembangkit Energi </h1><hr><br>

<div class="row">
  
<div class="col-sm-12">
  
  <h4 class="h4">KOMPETENSI UTAMA</h4>
<p class="p">Lulusan program ini memiliki kapasitas dalam bidang dalam mengevaluasi, mengelola operasional dan pemeliharaan pada unit pembangkit tenaga listrik (power plant engineer), memonitor, menganalisa, dan menanggulangi masalah operasi, serta melaksanakan pemeliharaan pada unit pembangkit tenaga listrik (power plant technician), mampu mempersiapkan, menjalankan dan mengalisa stabilitas sistem, serta mengoperasikan peralatan gardu induk dan transmisi tenaga listrik (power transmission & distribution engineer), merencanakan dan menerapkan standar pemeliharaan, melaksanakan pemeliharaan, menganalisa dan menanggulangi masalah pada sistem kelistrikan (electrical technician).</p>
</div>

<div class="col-sm-6">

</div>



</div>

    
  <br>

  <h4 class="h4">PROSPEK KERJA</h4>

  <ol type="1">
    <li>
Supervisor pembangkit energi listrik
    </li>

    <li>
 Teknisi operasi dan pemeliharaan pembangkit
    </li>
    <li>Teknisi operasi dan pemeliharaan penyaluran tenaga listrik</li>
    <li>Konsultan tenaga listrik</li>
    <li>Teknisi listrik di berbagai utilitas industri manufaktur, utilitas gedung, perhotelan, dan kawasan industri</li>

  
    </ol>
<br>



  <h4 class="h4">KOMPETENSI LULUSAN</h4>

  <ul >
    <li>
Mampu menyiapkan data evaluasi operasi pembangkitan
    </li>

    <li>
Mampu melakukan evaluasi berdasarkan data hasil analisa inspeksi
    </li>

    <li>
Mampu mengelola operasional dan pemeliharaan pembangkit
    </li>

    <li>
Mampu menganalisis pelaksanaan pengoperasian unit pembangkit listrik
    </li>

    <li>
Mampu memonitor, menganalisa dan menanggulangi masalah operasi, melaksanakan pengujian keandalan operasi unit, serta membuat laporan
  </li>

      <li>
Mampu merencanakan dan menerapkan standar pemeliharaan, melaksanakan pemeliharaan, menganalisa dan menanggulangi masalah pada pembangkit listrik
    </li>

      <li>
Mampu mengoperasikan peralatan gardu induk dan transmisi, manuver jaringan SUTM, sistem SCADA untuk mengendalikan operasi jaringan tegangan menengah
    </li>

    <li>
Mampu menkorelasikan, mendiagnosis, dan menyimpulkan berbagai masalah pengelolaan dan penanganan bidang pembagkitan dan penyaluranenergi listrik sesuai dengan aturan-aturan pembangkitan dan standar-standar ketenagalistrikan
  </li>
    </ul>
<br>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Kurikulum-D3-Teknik-Elektronika-Polibatam.pdf"> <i class="fa-solid fa-download"></i> Download Kurikulum
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Sertifikat-Akreditasi-EL-2020-2025.pdf"> <i class="fa-solid fa-download"></i> Download Akreditasi
</a>

<a class="btn btn-default" id="tomboldownload" href="https://www.polibatam.ac.id/wp-content/uploads/2021/10/Elekronika-Manufaktur-1.pdf">
  <i class="fa-solid fa-download"></i> Download Mata Kuliah
</a>






<br><br>
<?php break;?>
<?php 

  default:
    require ("../../home.php");
    break;
 ?>
 <?php } ?>