const container = document.getElementById('container');
localStorage.setItem('isMobile', false)
toggleContainer = function() {
    container.classList.toggle("right-panel-active");
}

toggleContainerMobile = function() {
    container.classList.toggle("right-panel-active");

}

const user = document.querySelectorAll('.user');

for (let i = 0; i < user.length; i++) {
    user[i].onclick = function() {
        let j = 0;
        while (j < user.length) {
            user[j++].className = "user"
        }
        user[i].className = "user selected"
    }
}

//#region Setup Responsive
const formcontainer1 = document.getElementById('form-container1');
const formcontainer2 = document.getElementById('form-container2');
const overlaycontainer = document.getElementById('overlay-container');
if (window.innerWidth < 850) {
    container.classList.add('mobile')
    formcontainer1.classList.add('mobile')
    formcontainer2.classList.add('mobile')
    overlaycontainer.classList.add('hide')
} else {
    container.classList.remove('mobile')
    formcontainer1.classList.remove('mobile')
    formcontainer2.classList.remove('mobile')
    overlaycontainer.classList.remove('hide')
}

window.addEventListener('resize', function() {
    if (window.innerWidth < 850) {
        container.classList.add('mobile')
        formcontainer1.classList.add('mobile')
        formcontainer2.classList.add('mobile')
        overlaycontainer.classList.add('hide')
    } else {
        container.classList.remove('mobile')
        formcontainer1.classList.remove('mobile')
        formcontainer2.classList.remove('mobile')
        overlaycontainer.classList.remove('hide')
    }
})

//#endregion

const ormawa = document.getElementById('login-ormawa')
const pembina = document.getElementById('login-pembina')
const kemahasiswaan = document.getElementById('login-kemahasiswaan')
const title = document.getElementById('title-login')
const description = document.getElementById('description-login')
ormawa.addEventListener('click', function() {
    title.innerHTML = 'Ormawa'
    description.innerHTML = 'Melihat Status Berkas Dan Update Profile Ormawa mu'
})

pembina.addEventListener('click', function() {
    title.innerHTML = 'Pembina'
    description.innerHTML = 'Melihat Berkas Dan Menyetujui Berkas Ormawa'
})

pembina.addEventListener('click', function() {
    title.innerHTML = 'Pembina'
    description.innerHTML = 'Melihat Berkas Dan Menyetujui Berkas Ormawa'
})

kemahasiswaan.addEventListener('click', function() {
    title.innerHTML = 'Kemahasiswaan'
    description.innerHTML = 'Melihat Berkas Dan Menyetujui Berkas Ormawa'
})