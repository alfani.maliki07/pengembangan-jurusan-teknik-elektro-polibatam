    function dropHandler(ev) {
        console.log('File(s) dropped');
        ev.preventDefault();
        var fileName = [];
        for (var i = 0; i < ev.dataTransfer.files.length; i++) {
            fileName.push(ev.dataTransfer.files[i].name)
        }
        var htmlFileName = fileName.join(" <br> ")
        fileTxt.innerHTML = htmlFileName
        fileLapKegiatan.files = ev.dataTransfer.files
    }

    function dragOverHandler(ev) {
        console.log('File(s) in drop zone');

        // Prevent default behavior (Prevent file from being opened)
        ev.preventDefault();
    }