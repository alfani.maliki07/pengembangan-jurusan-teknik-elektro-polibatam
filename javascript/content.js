var fileLapKegiatan = document.getElementById('fileLapKegiatan')
var fileTxt = document.getElementById('file-text')

function onClickFileLapKegiatan() {
    fileLapKegiatan.click()
}

var dropZone = document.getElementById("drop_zone")
dropZone.addEventListener('click', function() {
    fileLapKegiatan.click()
})

function onChangeFile(file) {
    var fileName = [];
    for (var i = 0; i < file.files.length; i++) {
        fileName.push(file.files[i].name)
    }
    var htmlFileName = fileName.join(" <br> ")
    fileTxt.innerHTML = htmlFileName
    fileLapKegiatan.files = file.files
}