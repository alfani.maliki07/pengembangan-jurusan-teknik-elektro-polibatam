function toggleSideMenu() {
    let sidebar = document.querySelector(".navigation")
    let content = document.querySelector(".content")
    let toggleColapse = document.querySelector(".toggle-colapse")
    let title = document.querySelectorAll(".title-menu")
    sidebar.classList.toggle('mobile')
    content.classList.toggle('mobile')
    toggleColapse.classList.toggle('show')
    for (let i = 0; i < title.length; i++) {
        title[i].classList.toggle('hide')
    }
}

let list = document.querySelectorAll('.list')
var uri = window.location.href.split('/')
var menu = uri[uri.length - 1].split('.')[0]
for (let i = 0; i < list.length; i++) {
    if (list[i].innerText.toLowerCase() == decodeURI(menu)) {
        document.title = list[i].innerText;
        list[i].className = "list active"
    } else {
        list[i].className = "list"
    }
}