var banner = document.getElementById('banner')
var imgBanner = document.getElementById('img-banner')

var imgProfile = document.getElementById('img-profile')
var profilePic = document.getElementById('profile-pic')


var imgWakil = document.getElementById('img-wakil')
var wakilPic = document.getElementById('profile-wakil')

var imgPembina = document.getElementById('img-pembina')
var pembinaPic = document.getElementById('profile-pembina')



function onClickEditBanner() {
    banner.click()
}

function onClickEditProfile() {
    profilePic.click()
}

function onClickEditWakil() {
    wakilPic.click()
}
function onClickEditPembina() {
    pembinaPic.click()
}


profilePic.onchange = function(evt) {
    var tgt = evt.target
    files = tgt.files;

    if (files && files.length) {
        var fr = new FileReader();
        fr.onload = function() {
            imgProfile.src = fr.result;
        }
        fr.readAsDataURL(files[0]);
    }
}

wakilPic.onchange = function(evt) {
    var tgt = evt.target
    files = tgt.files;

    if (files && files.length) {
        var fr = new FileReader();
        fr.onload = function() {
            imgWakil.src = fr.result;
        }
        fr.readAsDataURL(files[0]);
    }
}

pemnbinaPic.onchange = function(evt) {
    var tgt = evt.target
    files = tgt.files;

    if (files && files.length) {
        var fr = new FileReader();
        fr.onload = function() {
            imgPembina.src = fr.result;
        }
        fr.readAsDataURL(files[0]);
    }
}



banner.onchange = function(evt) {
    var tgt = evt.target
    files = tgt.files;

    if (files && files.length) {
        var fr = new FileReader();
        fr.onload = function() {
            imgBanner.src = fr.result;
        }
        fr.readAsDataURL(files[0]);
    }
}