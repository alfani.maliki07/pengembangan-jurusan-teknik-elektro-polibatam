<?php 
require "asset/function/function.php";


$batas = 8;

$halaman = isset($_GET['halaman']) ? (int)$_GET['halaman'] : 1;
$halaman_awal = ($halaman > 1) ? ($halaman * $batas) - $batas : 0;

$previous = $halaman - 1;
$next = $halaman + 1;
$querydata = "SELECT * FROM fasilitas";
$resultdata = $connection -> query($querydata);

$jumlah_data = mysqli_num_rows($resultdata);
$total_halaman = ceil($jumlah_data / $batas);

$result_fasilitas = $connection -> query( "select * from fasilitas limit $halaman_awal, $batas");
$nomor = $halaman_awal + 1;

?>
 <!DOCTYPE html>
 <html>
<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

 
<title>EL - Fasilitas</title>
  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">

  <!-- font awesome style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />



<link href="css/css/fontawesome.css" rel="stylesheet">
  <link href="css/css/brands.css" rel="stylesheet">
  <link href="css/css/solid.css" rel="stylesheet">


<style type="text/css">
  #tomboldownload{
    background: #1d345e;
    color: white;
  }
    #tomboldownload:hover{
    background: #253b63;
    color: silver;
    }








    .card{
      border: none;
      border-radius: 5px;
      box-shadow: 0px 1px 17px -10px;
    }

    .card:hover{
        margin-top: -9px;
        transition: 0.50s;
    }
       .card-title{
        font-size: 24px;

       }
       .card-title a{
        color: black;

       }
       .card-title a:hover{
      color: #011351; 
       }
       small{
        color: grey;
       }






</style>
</head>

<body >
  <!--<header>
    <a href="" class="logo">T-EL</a>
    <ul>
      <li>
        <a href="" class="nav-link">Pengumuman</a>
      </li>
      <li class="nav-item active">
        <a href="index.html" class="nav-link">Home</a>
      </li>
      <li>
        <a href="about.html" class="nav-link">Fasilitas</a>
          <ul>
            <li><a href="">Workspace</a></li>
            <li><a href="">Studio</a></li>
          </ul>
      </li>
      <li>
        <a href="service.html" class="nav-link">Program Studi</a>
          <ul>
            <li><a href="">Teknik Elektronika Manufaktur</a></li>
            <li><a href="">Teknik Elektronika</a></li>
            <li><a href="">Teknik Instrumentasi</a></li>
            <li><a href="">Teknik Mekatronika</a></li>
            <li><a href="">Teknologi Rekayasa Pembangkit Energi</a></li>
            <li><a href="">Teknologi Rekayasa Robotika</a></li>
          </ul>
      </li>
      <li><a href="" class="nav-link">Dosen</a></li>
      <li><a href="" class="nav-link">Prestasi</a></li>
    </ul>
  </header>-->
<?php   include "asset/function/header.php" ?>
<br><br>



<!-- heading -->
<div class="container">
<h2 class="h2 text-left" style="color: #011351;">
<b>FASILITAS</b>
</h2>
<hr style="width: 100px; margin-left: -1px; border:1px solid black;">
<p class="p text-left" size>
Fasilitas yang ada di jurusan Teknik Elektro
</p>
</div>
<!-- akhir heading -->



 



<br><br>

<!-- pengumuman -->
<div class="container">
  

  <div class="row">

<?php while ($data = $result_fasilitas -> fetch_assoc()):?>
    <div class="col-sm-4">
    
  <div class="card" >

  <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/fasilitas/<?php echo $data['gambar1'] ?>" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
    <img src="images/fasilitas/<?php echo $data['gambar2'] ?>" class="d-block w-100" alt="...">
    </div>
    
  </div>
</div>






  <div class="card-body">
    <div class="card-title">
      <a href="detail-fasilitas.php?id_fasilitas=<?php echo $data['id_fasilitas'] ?>">
      <b> <?php echo $data['nama_fasilitas'] ?></b></a>
    </div>
    <small>Admin</small>
<hr>
<?php echo $data['deskripsi_fasilitas'] ?>
  </div>
  </div>
<br>
    </div>

<?php endwhile; ?>
  </div>
<br>  

  <div class="row">

 <nav aria-label="...">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="<?php if ($halaman > 1) {
                                                            echo '?link=2&halaman=$Previous';
                                                        } ?>">Previous</a>
                        </li>
                        <?php
                        for ($x = 1; $x <= $total_halaman; $x++) {
                        ?>
                            <li class="page-item"><a class="page-link" href="?link=2&halaman=<?php echo $x ?>"><?php echo $x; ?></a></li>
                        <?php
                        }
                        ?>
                        <li class="page-item">
                            <a class="page-link" <?php if ($halaman < $total_halaman) {
                                                        echo "href='?link=2&halaman=$next'";
                                                    } ?>>Next</a>
                        </li>
                    </ul>
                </nav>


  </div>


</div>

<!-- akhir pengumuman -->


<br>  <br>  
    <!-- footer section -->
    <section class="footer_section">
      <div class="container">
        <p>
          &copy; <span id="displayYear"></span> Politeknik Negeri Batam
        </p>
      </div>
    </section>
    <!-- footer section -->
    <!-- footer section -->
    <!-- jQery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script>
      $(document).ready(function () {
        $(".sub-btn").click(function () {
          $(this).next(".sub-menu").slideToggle();
        });

        $(".more-btn").click(function () {
          $(this).next(".more-menu").slideToggle();
        });
      });
    </script>
    <script>
      window.addEventListener("scroll", function () {
        var header = document.querySelector("header");
        header.classList.toggle("sticky", window.scrollY > 0);
      })
    </script>
    <!-- popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- owl slider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
    </script>
    <!-- End Google Map -->



 </body>
 </html>