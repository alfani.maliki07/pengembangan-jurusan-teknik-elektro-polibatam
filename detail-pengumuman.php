<?php 
require "asset/function/function.php";

if (!isset($_GET['id_pengumuman'])) {
	header("Location:pengumuman.php");
}
$id_pengumuman = $_GET['id_pengumuman'];
$result = $connection -> query("SELECT * FROM pengumuman WHERE id_pengumuman = '$id_pengumuman'");
$data2 = $result -> fetch_assoc();

 ?>
 <!DOCTYPE html>
 <html>
<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

 

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">

  <!-- font awesome style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />



<link href="css/css/fontawesome.css" rel="stylesheet">
  <link href="css/css/brands.css" rel="stylesheet">
  <link href="css/css/solid.css" rel="stylesheet">


<style type="text/css">
  #tomboldownload{
    background: #1d345e;
    color: white;
  }
    #tomboldownload:hover{
    background: #253b63;
    color: silver;
    }








    .card{
      border: none;
      border-radius: 5px;
      box-shadow: 0px 1px 17px -10px;
    }

  
       .card-title{
        font-size: 24px;

       }
       .card-title a{
        color: black;

       }
       .card-title a:hover{
      color: #011351; 
       }
       small{
        color: grey;
       }







</style>
</head>

<body >
  <!--<header>
    <a href="" class="logo">T-EL</a>
    <ul>
      <li>
        <a href="" class="nav-link">Pengumuman</a>
      </li>
      <li class="nav-item active">
        <a href="index.html" class="nav-link">Home</a>
      </li>
      <li>
        <a href="about.html" class="nav-link">Fasilitas</a>
          <ul>
            <li><a href="">Workspace</a></li>
            <li><a href="">Studio</a></li>
          </ul>
      </li>
      <li>
        <a href="service.html" class="nav-link">Program Studi</a>
          <ul>
            <li><a href="">Teknik Elektronika Manufaktur</a></li>
            <li><a href="">Teknik Elektronika</a></li>
            <li><a href="">Teknik Instrumentasi</a></li>
            <li><a href="">Teknik Mekatronika</a></li>
            <li><a href="">Teknologi Rekayasa Pembangkit Energi</a></li>
            <li><a href="">Teknologi Rekayasa Robotika</a></li>
          </ul>
      </li>
      <li><a href="" class="nav-link">Dosen</a></li>
      <li><a href="" class="nav-link">Prestasi</a></li>
    </ul>
  </header>-->
  <?php   include "asset/function/header.php" ?>


<br><br>


<!-- heading -->
<div class="container">
<h2 class="h2 text-left" style="color: #011351;">
<b>PENGUMUMAN</b>
</h2>
<hr style="width: 100px; margin-left: -1px; border:1px solid black;">
<p class="p text-left" size>
Pengumuman terkait jurusan Teknik Elektro
</p>
</div>
<!-- akhir heading -->



 



<br><br>

<!-- pengumuman -->
<div class="container">
  

  <div class="row">

    <div class="col-sm-12">
    
  <div class="card" >
  <div class="card-body">
    <div class="card-title"><b> <?php echo $data2['judul'] ?></b></div>
    <small>Posted Admin, <?php echo $data2['tanggal'] ?></small>
<hr>

<?php echo $data2['headline'] ?>

<br>
<?php if (!$data2['file'] == ""): ?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data2['id_pengumuman'] ?>">
  Lihat Dokumen
</button>  
<?php endif ?>

  </div>
  </div>

    </div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $data2['id_pengumuman'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <?php 
$id_view = $data2['id_pengumuman'];

$resultview = $connection -> query("SELECT * FROM pengumuman WHERE id_pengumuman ='$id_view'");
$view = $resultview ->fetch_assoc();
   ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary"><?php echo $view['judul'] ?></button>
      </div>
    </div>
  </div>
</div>
  </div>
<br>  



</div>

<!-- akhir pengumuman -->


<br>  <br>  <br>
    <!-- footer section -->
    <section class="footer_section">
      <div class="container">
        <p>
          &copy; <span id="displayYear"></span> Politeknik Negeri Batam
        </p>
      </div>
    </section>
    <!-- footer section -->
    <!-- footer section -->
    <!-- jQery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script>
      $(document).ready(function () {
        $(".sub-btn").click(function () {
          $(this).next(".sub-menu").slideToggle();
        });

        $(".more-btn").click(function () {
          $(this).next(".more-menu").slideToggle();
        });
      });
    </script>
    <script>
      window.addEventListener("scroll", function () {
        var header = document.querySelector("header");
        header.classList.toggle("sticky", window.scrollY > 0);
      })
    </script>
    <!-- popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- owl slider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
    </script>
    <!-- End Google Map -->



 </body>
 </html>