<?php 
require "asset/function/function.php";

$result = $connection -> query("SELECT * FROM dosen");

 ?>

 <!DOCTYPE html>
 <html>
<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

 

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">

  <!-- font awesome style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link href="css/font-awesome.min.css" rel="stylesheet" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />



<link href="css/css/fontawesome.css" rel="stylesheet">
  <link href="css/css/brands.css" rel="stylesheet">
  <link href="css/css/solid.css" rel="stylesheet">


<style type="text/css">
  #tomboldownload{
    background: #1d345e;
    color: white;
  }
    #tomboldownload:hover{
    background: #253b63;
    color: silver;
    }
    .card{
       height: 500px; border-radius:10px 10px ; 
       box-shadow: 0px 0px 20px -13px black;
      margin-top: 10px;
    }
body{

}
</style>
</head>

<body >
  <!--<header>
    <a href="" class="logo">T-EL</a>
    <ul>
      <li>
        <a href="" class="nav-link">Pengumuman</a>
      </li>
      <li class="nav-item active">
        <a href="index.html" class="nav-link">Home</a>
      </li>
      <li>
        <a href="about.html" class="nav-link">Fasilitas</a>
          <ul>
            <li><a href="">Workspace</a></li>
            <li><a href="">Studio</a></li>
          </ul>
      </li>
      <li>
        <a href="service.html" class="nav-link">Program Studi</a>
          <ul>
            <li><a href="">Teknik Elektronika Manufaktur</a></li>
            <li><a href="">Teknik Elektronika</a></li>
            <li><a href="">Teknik Instrumentasi</a></li>
            <li><a href="">Teknik Mekatronika</a></li>
            <li><a href="">Teknologi Rekayasa Pembangkit Energi</a></li>
            <li><a href="">Teknologi Rekayasa Robotika</a></li>
          </ul>
      </li>
      <li><a href="" class="nav-link">Dosen</a></li>
      <li><a href="" class="nav-link">Prestasi</a></li>
    </ul>
  </header>-->
  
  <!-- header -->
<?php include "asset/function/header.php" ?>
<!-- akhir header -->
<br><br>



<!-- heading -->

<h1 class="h1 text-center" style="color: #011351;">
<b>STRUKTURAL TEKNIK ELEKTRO</b>
</h1>
<hr style="width: 100px; border:1px solid black;">
<p class="p text-center" size>
Pengelola Jurusan Teknik Informatika terdiri dari Ketua Jurusan, Sekretaris Jurusan, Ketua Program Studi, dan Koordinator Laboran. Berikut Pengelola Jurusan Teknik Informatika	
</p>
<!-- akhir heading -->



 



<br><br>





<!-- daftar dosen -->


<div class="container">	
<div class="row" >



<?php   while ( $data = $result -> fetch_assoc() ):?>
	<div class="col-sm-6" style="" id="kartu" >
		<div class="card"   >
  <div class="card-body" >


<div class="row" >
<div class="col-sm-4">
	<img src="images/dosen/<?php echo $data['foto'] ?>" alt="..." style="border-radius: 465px; width: 150px;">
</div>
  	<div class="col-sm-8" >

   <h5 class="card-title" style="color: #455d86;"> <b><?php echo $data['nama'] ?></b></h5>
    <p class="card-text"><?php echo  $data['jabatan'] ?></p>
    <hr style="width: 50px; margin-left:-1px; border:1px solid black;">
    <p class="card-text"> <b>NIDN/NIK</b>   : <?php echo  $data['nidn'] ?> <br> 
      <b>Program Studi</b> : <?php echo $data['prodi'] ?> <br> 
      <b>Pangkat/Golongan</b> : <?php echo  $data['pangkat'] ?> <br> 
      <b>Pendidikan Terakhir</b> : <?php echo $data['pendidikan_terakhir'] ?> <br> 
      <b>Email</b> : <?php echo $data['email'] ?></p> 
    
    <h5 class="card-title" style="color: #455d86;"> <b> Riwayat Pendidikan</b></h5>
    <hr style="width: 50px; margin-left:-1px; border:1px solid black;">
    <p class="card-text"> <?php echo  $data['riwayat'] ?></p>
    
    <h5 class="card-title" style="color: #455d86;"> <b> Bidang</b></h5>
    <hr style="width: 50px; margin-left:-1px; border:1px solid black;">
    <p class="card-text"> <?php echo  $data['bidang'] ?></p>
  </div>


</div>


</div>
	</div>
</div>

<?php   endwhile; ?>



</div>

</div>

<br>


<!-- akhir daftar dosen -->


<br>






  <div class="footer_bg">



    <!-- info section -->
    <section class="info_section ">
      <div class="container">
        <div class="row">
          <div class="col-md-3 mb-4 mb-md-0 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_info">
              <h5>
                Informations
              </h5>
              <p>
                ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
              </p>
            </div>
          </div>
          <div class="col-md-3 mb-4 mb-md-0 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_contact">
              <h5>
                About
              </h5>
              <div>
                <div class="img-box">
                  <img src="images/location-white.png" width="18px" alt="">
                </div>
                <p>
                  Address
                </p>
              </div>
              <div>
                <div class="img-box">
                  <img src="images/telephone-white.png" width="12px" alt="">
                </div>
                <p>
                  +01 1234567890
                </p>
              </div>
              <div>
                <div class="img-box">
                  <img src="images/envelope-white.png" width="18px" alt="">
                </div>
                <p>
                  demo@gmail.com
                </p>
              </div>
            </div>
          </div>
          <!-- <div class="col-md-3 mb-4 mb-md-0 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_insta">
              <h5>
                Instagram
              </h5>
              <div class="insta_container">
                <div>
                  <a href="">
                    <div class="insta-box b-1">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                  <a href="">
                    <div class="insta-box b-2">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                </div>

                <div>
                  <a href="">
                    <div class="insta-box b-3">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                  <a href="">
                    <div class="insta-box b-4">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                </div>
                <div>
                  <a href="">
                    <div class="insta-box b-3">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                  <a href="">
                    <div class="insta-box b-4">
                      <img src="images/insta.png" alt="">
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex d-md-block flex-column align-items-center text-center text-md-left ">
            <div class="info_form ">
              <h5>
                Newsletter
              </h5>
              <form action="">
                <input type="email" placeholder="Enter your email">
                <button>
                  Subscribe
                </button>
              </form>
              <--<div class="social_box">
                <a href="">
                  <img src="images/fb.png" alt="">
                </a>
                <a href="">
                  <img src="images/twitter.png" alt="">
                </a>
                <a href="">
                  <img src="images/linkedin.png" alt="">
                </a>
                <a href="">
                  <img src="images/youtube.png" alt="">
                </a>
              </div>
            </div>
          </div>-->
        </div>
      </div>
    </section>

    <!-- end info_section -->

    <!-- footer section -->
    <section class="footer_section">
      <div class="container">
        <p>
          &copy; <span id="displayYear"></span> Politeknik Negeri Batam
        </p>
      </div>
    </section>
    <!-- footer section -->
    <!-- footer section -->
    <!-- jQery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script>
      $(document).ready(function () {
        $(".sub-btn").click(function () {
          $(this).next(".sub-menu").slideToggle();
        });

        $(".more-btn").click(function () {
          $(this).next(".more-menu").slideToggle();
        });
      });
    </script>
    <script>
      window.addEventListener("scroll", function () {
        var header = document.querySelector("header");
        header.classList.toggle("sticky", window.scrollY > 0);
      })
    </script>
    <!-- popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- owl slider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!-- custom js -->
    <script type="text/javascript" src="js/custom.js"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
    </script>
    <!-- End Google Map -->



 </body>
 </html>